
set(EMIPLIB_FOUND 1)


find_package(Qt5Gui REQUIRED)
find_package(Qt5Widgets REQUIRED)
find_package(Qt5OpenGL REQUIRED)
find_package(Qt5Multimedia REQUIRED)

set(EMIPLIB_INCLUDE_DIRS  "C:/Users/user/src/lib/include" "C:/Qt/5.11.0/mingw53_32/include/" "C:/Qt/5.11.0/mingw53_32/include/QtGui" "C:/Qt/5.11.0/mingw53_32/include/QtCore" "C:/Qt/5.11.0/mingw53_32/.//mkspecs/win32-g++" "C:/Qt/5.11.0/mingw53_32/include//QtANGLE" "C:/Qt/5.11.0/mingw53_32/include/QtWidgets" "C:/Qt/5.11.0/mingw53_32/include/QtOpenGL" "C:/Qt/5.11.0/mingw53_32/include/QtMultimedia" "C:/Qt/5.11.0/mingw53_32/include/QtNetwork")

set(EMIPLIB_LIBRARIES  "-LC:/Users/user/src/lib/lib" "-lemip" "-LC:/Users/user/src/lib/lib" "-ljthread" "-LC:/Users/user/src/lib/lib" "-ljrtp" "-LC:/Users/user/src/lib/lib" "-ljthread" "ws2_32" "winmm" "Qt5::Gui" "Qt5::Widgets" "Qt5::OpenGL" "Qt5::Multimedia")

