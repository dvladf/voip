#ifndef LOGIN_H
#define LOGIN_H

#include <QComboBox>
#include <QDialog>
#include <QHostAddress>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>

#include <string>

class Login : public QDialog
{
    std::vector<QHostAddress> localAddresses;
    std::vector<QHostAddress> broadcastAddresses;
    
    std::string& name;

    QHostAddress& localAddress;
    QHostAddress& broadcastAddress;
    
    QLabel* nameLabel;
    QLineEdit* nameEdit;
    QLabel* networkLabel;
    QComboBox* networkComboBox;
    QPushButton* button;
    
public:
    Login(std::string& name, QHostAddress& localAddress, QHostAddress& broadcastAddress);

private:
    void initUi();
    void getBroadcastAddresses();
    void entered();
};

#endif /* LOGIN_H */
