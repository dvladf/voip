#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLineEdit>
#include <QListView>
#include <QPushButton>
#include <QStringListModel>
#include <QTextEdit>
#include <QTime>
#include <QTimer>
#include <QUdpSocket>

#include <unordered_map>
#include <string>

class ChatView : public QTextEdit
{
public:
    ChatView();
    void appendRecieve(const QString& sender, const QString& message);
    void appendSend(const QString& message);
    void appendJoin(const QString& name);
    void appendRelease(const QString& name);

private:
    void align(Qt::AlignmentFlag alignment);
};

class ConnectionsView : public QListView
{
public:
    ConnectionsView();
    void add(const QString& connection);
    void remove(const QString& connection);
    
private:
    QStringListModel model;
};

class MainWindow : public QMainWindow
{
public:
    MainWindow(const std::string& name,
	       const QHostAddress& localAddress,
	       const QHostAddress& broadcastAddress);

private:
    void sendState(const std::string& op);
    void sendMessage();
    void checkConnections();
    void readyRead();
    void initUi();
    
private:
    struct Connection {
	QHostAddress address;
        QTime timestamp;
    };
    
    std::string name;
    QHostAddress localAddress;
    QHostAddress broadcastAddress;

    static constexpr int TIMEOUT = 3000;
    std::unordered_map<std::string, Connection> connections;
    
    ChatView* chatView;
    ConnectionsView* connectionsView;
    QLineEdit* edit;
    QPushButton* sendButton;
    QPushButton* callButton;
    
    QTimer* sendAliveTimer;
    QTimer* checkConnectionsTimer;
    
    QUdpSocket* socket;
};

#endif /* MAINWINDOW_H */
