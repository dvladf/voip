#include <QApplication>
#include <QDebug>

#include "mainwindow.h"
#include "login.h"

int main(int argc, char** argv)
{
    QApplication app(argc, argv);

    std::string name;

    QHostAddress localAddress;
    QHostAddress broadcastAddress;
    
    Login login(name, localAddress, broadcastAddress);
    int rc = login.exec();

    if (rc == QDialog::Rejected) {
	return 1;
    }
    
    MainWindow w(name, localAddress, broadcastAddress);
    w.show();
    return app.exec();
}
