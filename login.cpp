#include "login.h"

#include <QApplication>
#include <QDebug>
#include <QNetworkInterface>
#include <QLayout>
#include <QMessageBox>
#include <QDebug>

#include <vector>

Login::Login(std::string& name, QHostAddress& localAddress, QHostAddress& broadcastAddress) :
    QDialog(), name(name), localAddress(localAddress), broadcastAddress(broadcastAddress)
{
    getBroadcastAddresses();
    initUi();

    connect(button, &QPushButton::clicked, this, &Login::entered);
    connect(nameEdit, &QLineEdit::returnPressed, this, &Login::entered);
}

void Login::getBroadcastAddresses()
{
    auto interfaces = QNetworkInterface::allInterfaces();
    
    for (const auto& interface : interfaces) {
	if (interface.hardwareAddress() != "00:00:00:00:00:00") {
	    auto entries = interface.addressEntries();
	    for (const auto& entry : entries) {
		QHostAddress addr = entry.broadcast();
		
		if (!addr.toString().isEmpty()) {
		    qDebug() << entry.ip();
		    localAddresses.push_back(entry.ip());
		    broadcastAddresses.push_back(addr);;
		} 
	    }
	}        
    }
}

void Login::entered()
{
    if (nameEdit->text().isEmpty()) {
	QMessageBox::warning(this, tr("Warning"), tr("Login is empty"), QMessageBox::Ok);
    } else {
	name = nameEdit->text().toStdString();
	int index = networkComboBox->currentIndex();
	broadcastAddress = broadcastAddresses[index];
	localAddress = localAddresses[index];
        accept();
    }
}

void Login::initUi()
{
    setWindowTitle("Login");
    
    auto layout = new QVBoxLayout(this);
    setLayout(layout);
    
    nameLabel = new QLabel("Login:", this);
    nameEdit = new QLineEdit(this);
    networkLabel = new QLabel("Network:", this);
    networkComboBox = new QComboBox(this);

    for (const auto& addr : broadcastAddresses) {
	networkComboBox->addItem(addr.toString());
    }
    
    button = new QPushButton(tr("Next"), this);
    button->setFixedWidth(80);
    button->setDefault(false);
    button->setAutoDefault(false);
    
    layout->addWidget(nameLabel);
    layout->addWidget(nameEdit);
    layout->addWidget(networkLabel);
    layout->addWidget(networkComboBox);
    layout->addWidget(button);
}
