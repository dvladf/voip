#include "mainwindow.h"

#include <QLayout>
#include <QScrollBar>

MainWindow::MainWindow(const std::string& name,
		       const QHostAddress& localAddress,
		       const QHostAddress& broadcastAddress) :
    QMainWindow(),
    name(name),
    localAddress(localAddress),
    broadcastAddress(broadcastAddress)
{
    initUi();

    sendAliveTimer = new QTimer(this);
    checkConnectionsTimer = new QTimer(this);
    
    socket = new QUdpSocket(this);
    socket->bind(QHostAddress::Any, 14000);
    sendState("CONNECT");
    
    connect(sendAliveTimer, &QTimer::timeout, [this](){ MainWindow::sendState("ALIVE"); });
    sendAliveTimer->start(500);

    connect(checkConnectionsTimer, &QTimer::timeout, this, &MainWindow::checkConnections);
    checkConnectionsTimer->start(TIMEOUT);
    
    connect(socket, &QUdpSocket::readyRead, this, &MainWindow::readyRead);
    connect(edit,  &QLineEdit::returnPressed, this, &MainWindow::sendMessage);
    connect(sendButton, &QPushButton::clicked, this, &MainWindow::sendMessage);
}

void MainWindow::sendState(const std::string& op)
{
    const std::string buffer = "EVMp_" + op + "_" + std::to_string(name.size()) + "_" + name;
    socket->writeDatagram(buffer.data(), buffer.size(), broadcastAddress, 14000);
}

void MainWindow::sendMessage()
{
    if (edit->text().isEmpty()) {
	return;
    }

    const std::string& msg = edit->text().toStdString();
    const std::string buffer = "EVMp_SENDMSG_" + std::to_string(msg.size()) + "_" + msg;
    socket->writeDatagram(buffer.data(), buffer.size(), broadcastAddress, 14000);
    chatView->appendSend(edit->text());
    edit->clear();
}

void MainWindow::checkConnections()
{
    std::vector<std::unordered_map<std::string, Connection>::iterator> losts;

    for (auto it = connections.begin(); it != connections.end(); ++it) {
	if (it->second.timestamp.elapsed() > TIMEOUT) {
	    losts.push_back(it);
	}
    }

    for (auto lost : losts) {
	chatView->appendRelease(lost->first.c_str());
	connectionsView->remove(lost->first.c_str());
	connections.erase(lost);
    }
}

void MainWindow::readyRead()
{
    static constexpr int MAX_BUFFER_SIZE = 512;

    std::string buffer;
    buffer.resize(MAX_BUFFER_SIZE);
    
    QHostAddress sender;
    uint16_t port;
    socket->readDatagram(&buffer[0], MAX_BUFFER_SIZE, &sender, &port);
    
    if (sender.isEqual(localAddress)) {
	return;
    }
    
    size_t delimIndex = buffer.find('_');
    
    if (delimIndex == std::string::npos)
	return;
    
    if (buffer.compare(0, delimIndex, "EVMp") == 0) {
	size_t nextDelimIndex = buffer.find('_', delimIndex+1);

	if (nextDelimIndex == std::string::npos)
	    return;

	std::string op(buffer.begin()+delimIndex+1, buffer.begin()+nextDelimIndex);

	delimIndex = nextDelimIndex;
        nextDelimIndex = buffer.find('_', delimIndex+1);

	if (nextDelimIndex == std::string::npos)
	    return;

        const char* msg = buffer.data() + nextDelimIndex + 1;
	const int len = std::stoi(std::string(buffer.begin()+delimIndex+1, buffer.begin()+nextDelimIndex));
	buffer[nextDelimIndex + len + 1] = '\0';        
	
	if (op == "CONNECT") {
	    connections[msg] = { sender, QTime::currentTime() };
	    chatView->appendJoin(msg);
	    connectionsView->add(msg);
	} else if (op == "ALIVE") {
	    if (connections.find(msg) == connections.end()) {
		connections[msg] = { sender, QTime::currentTime() };
		connectionsView->add(msg);
	    } else {
		connections[msg].timestamp = QTime::currentTime();
	    }
	} else if (op == "SENDMSG") {
	    for (auto iter = connections.begin(); iter != connections.end(); ++iter) {
		if (iter->second.address == sender) {
		    chatView->appendRecieve(iter->first.c_str(), msg);
		}
	    }
	}
    }
}

void MainWindow::initUi()
{
    setFixedSize(800, 500);
    setWindowTitle("voip messenger");
    
    QWidget* centralWidget = new QWidget(this);
    setCentralWidget(centralWidget);
    
    QVBoxLayout* mainLayout = new QVBoxLayout;
    QHBoxLayout* chatLayout = new QHBoxLayout;
    QHBoxLayout* editLayout = new QHBoxLayout;
    
    centralWidget->setLayout(mainLayout);

    connectionsView = new ConnectionsView;
    connectionsView->setFixedWidth(200);
    chatView = new ChatView;
    chatView->setReadOnly(true);

    chatLayout->addWidget(connectionsView);
    chatLayout->addWidget(chatView);
    mainLayout->addLayout(chatLayout);

    edit = new QLineEdit(this);
    edit->setPlaceholderText(tr("write a message..."));
    sendButton = new QPushButton(tr("Send"), this);
    callButton = new QPushButton(tr("Call"), this);
    
    editLayout->addWidget(edit);
    editLayout->addWidget(sendButton);
    editLayout->addWidget(callButton);
    mainLayout->addLayout(editLayout);
    
    edit->setFocus(Qt::OtherFocusReason);
}

ChatView::ChatView() : QTextEdit()
{
    setReadOnly(true);
}

void ChatView::appendJoin(const QString& name)
{
    QString line = "<font color=\"blue\"><b>" + name +
	tr(" has joined") + "</b></font>";
    
    append(line);
    align(Qt::AlignLeft);
}

void ChatView::appendRelease(const QString &name)
{
    QString line = "<font color=\"red\"><b>" + name +
	tr(" has released") + "</b></font>";
    
    append(line);
    align(Qt::AlignLeft);
}

void ChatView::appendRecieve(const QString &sender, const QString &message)
{
    QString line = "<b>" + sender + " > </b>" + message;
    append(line);
    align(Qt::AlignLeft);
    verticalScrollBar()->setValue(verticalScrollBar()->maximum());
}

void ChatView::appendSend(const QString &message)
{
    QString line = "<b>" + message + "</b>";
    append(line);
    align(Qt::AlignRight);
    verticalScrollBar()->setValue(verticalScrollBar()->maximum());
}

void ChatView::align(Qt::AlignmentFlag alignment)
{
    QTextCursor cursor = textCursor();
    QTextBlockFormat format = cursor.blockFormat();
    format.setAlignment(alignment);
    cursor.mergeBlockFormat(format);
    setTextCursor(cursor);
}

ConnectionsView::ConnectionsView()
{
    setModel(&model);
}

void ConnectionsView::add(const QString& connection)
{
    if (model.insertRows(0, 1)) {
	QModelIndex index = model.index(0, 0);
	model.setData(index, connection);
    }
}

void ConnectionsView::remove(const QString& connection)
{
    for (int i = 0; i < model.rowCount(); ++i) {
	QModelIndex index = model.index(i, 0);
	if (model.data(index, Qt::DisplayRole).toString() == connection) {
	    model.removeRows(i, 1);
	}
    }
}
