QT       += core gui widgets network

TARGET = voip
TEMPLATE = app

DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += c++11

INCLUDEPATH += ./lib/include
LIBS += -L$$PWD/lib/lib -ljthread -ljrtp -lemip

SOURCES += \
        main.cpp \
        mainwindow.cpp \
        login.cpp

HEADERS += \
        mainwindow.h \
        login.h
